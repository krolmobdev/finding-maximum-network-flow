﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace MaximumNetworkFlow
{
    public partial class MainForm : Form
    {
        public const string PROGRAM_NAME = "Поиск максимального потока в сети";

        private InputNetwork inputNetwork;

        private FordFalkersonAlgorithm seacher;

        private GraphController graphController;

        private BindingList<int> sourceValues = new BindingList<int>();
        private BindingList<int> targetValues = new BindingList<int>();


        public MainForm()
        {
            InitializeComponent();

            inputNetwork = new InputNetwork();

            //Привязываем данные списка ребер сети - inputNetwork к таблице ребер - dataGridViewПотоковаяСеть
            dataGridViewТаблицаРебер.Columns["ПерваяВершина"].DataPropertyName = "FirstVertex";
            dataGridViewТаблицаРебер.Columns["ВтораяВершина"].DataPropertyName = "SecondVertex";
            dataGridViewТаблицаРебер.Columns["ПропускнаяСпособность"].DataPropertyName = "Capacity";
            dataGridViewТаблицаРебер.Columns["Поток"].DataPropertyName = "Flow";

            dataGridViewТаблицаРебер.DataSource = inputNetwork.Edges;

            comboBoxИсток.DataSource = sourceValues;
            comboBoxСток.DataSource = targetValues;

            graphController = new GraphController();
        }

        /// <summary>
        /// Загрузка сети из текстового файла
        /// </summary>
        private void buttonЗагрузить_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Text Files(*.txt) | *.txt";

            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string[] lines = File.ReadAllLines(openDialog.FileName);

                    inputNetwork = new InputNetwork(lines);
                    dataGridViewТаблицаРебер.DataSource = inputNetwork.Edges;

                    //Обновляем выпадающие списки вершин таблицы ребер, истока и стока
                    ChangeItemsComboBoxColumnsOfDataGridViewТаблицаРебер();
                    ChangeItemsComboBoxesИстокAndСток();

                    //Задаем значения истока и стока
                    comboBoxИсток.SelectedIndex = inputNetwork.Source;
                    comboBoxСток.SelectedIndex = inputNetwork.Target;

                    labelЧислоВершин.Text = "Число вершин:" + inputNetwork.VertexCount;
                    textBoxЧислоВершин.Text = inputNetwork.VertexCount.ToString();

                    richTextBoxХодРешения.Clear();
                    richTextBoxРезультат.Clear();

                    buttonДобавитьРебро.Enabled = true;
                    buttonНайтиПоток.Enabled = true;

                    graphController.ResetLastLocation();
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(ex.Message, MainForm.PROGRAM_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, MainForm.PROGRAM_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Ищем максимальный поток в сети и отображаем его пошаговые значения
        /// </summary>
        private void buttonНайтиПоток_Click(object sender, EventArgs e)
        {
            try
            {
                if (inputNetwork.IsEqualsOfSourceAndTarget())
                {
                    throw new InputNetworkException("Исток и сток совпадают");
                }

                seacher = new FordFalkersonAlgorithm(new FlowNetwork(inputNetwork));


                //Ход решения

                richTextBoxХодРешения.Clear();
                richTextBoxХодРешения.AppendText(seacher.Protocol);
                richTextBoxХодРешения.AppendText("\n\nМинимальный разрез");

                string cutEdges = "";

                foreach (FlowEdge edge in seacher.MinCut)
                {
                    cutEdges += "\n(" + edge + ")";
                }
                richTextBoxХодРешения.AppendText(cutEdges);

                double minCutCapacity = seacher.MinCutCapacity;
                richTextBoxХодРешения.AppendText("\nПропускная cпособность разреза:" + minCutCapacity);


                //Результат

                richTextBoxРезультат.Clear();
                richTextBoxРезультат.AppendText("Величина максимального потока:" + seacher.Value);
                richTextBoxРезультат.AppendText("\nПропускная способность минимального разреза:" + minCutCapacity);


                //Анализ решения

                richTextBoxХодРешения.AppendText("\n\nАнализ решения");

                //Находим вершины с нарушенным балансом
                List<int> notBalancedVertices = seacher.LocalBalance();
                List<FlowEdge> notBalancedEdges = seacher.IsFeasibleFlow();

                if (notBalancedVertices.Count == 0)
                {
                    richTextBoxХодРешения.AppendText("\nВ каждой вершине (кроме истока и стока) приток равен оттоку");
                }
                else
                {
                    richTextBoxХодРешения.AppendText("\nВ вершинах: ");

                    foreach (int vertex in notBalancedVertices)
                    {
                        richTextBoxХодРешения.AppendText(vertex.ToString() + ", ");

                    }
                    richTextBoxХодРешения.AppendText(" - нарушен локальный баланс: приток не равен оттоку.");
                }
                if (notBalancedEdges.Count == 0)
                {
                    richTextBoxХодРешения.AppendText("\nВ каждом ребре поток не превышает его пропускную способность и неотрицателен.");
                }
                else
                {
                    richTextBoxХодРешения.AppendText("\nВ ребрах: ");

                    foreach (int vertex in notBalancedVertices)
                    {
                        richTextBoxХодРешения.AppendText(vertex.ToString() + " ");
                    }
                    richTextBoxХодРешения.AppendText(" задан недопустимый поток, который превышает пропускную способность ребра или отрицателен");
                }

                if (notBalancedVertices.Count == 0 && notBalancedEdges.Count == 0)
                {
                    richTextBoxХодРешения.AppendText("\nНайденный поток допустимый.");
                }
                else
                {
                    richTextBoxХодРешения.AppendText("\nНайденный поток недопустимый.");
                }

                graphController.Graph = new Graph(new InputNetwork(seacher.Network), pictureBoxГраф.Width, pictureBoxГраф.Height, 25);

                graphController.PlaceGraph(true);

                pictureBoxГраф.Invalidate();
            }
            catch (InputNetworkException ex)
            {
                MessageBox.Show(ex.Message, PROGRAM_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                string message = "В процессе выполнения алгоритма произошла ошибка: ";

                MessageBox.Show(message + ex.Message, PROGRAM_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Обработчик изменения числа вершин сети
        /// </summary>
        private void buttonЗадатьЧислоВершин_Click(object sender, EventArgs e)
        {
            try
            {
                int newVertexCount = int.Parse(textBoxЧислоВершин.Text);

                if (newVertexCount == 0)
                {
                    //Если пользователь задал нулевое число вершин

                    throw new Exception("Число вершин не должно быть равно нулю");
                }
                if (inputNetwork.VertexCount == 0)
                {
                    //Если число вершин в сети было равно нулю

                    buttonДобавитьРебро.Enabled = true;
                    buttonНайтиПоток.Enabled = true;

                    inputNetwork.VertexCount = newVertexCount;

                    ChangeItemsComboBoxColumnsOfDataGridViewТаблицаРебер();
                }
                else if (newVertexCount == inputNetwork.VertexCount)
                {
                    //Если число вершин в сети не изменилось

                    return;
                }
                else if (newVertexCount < inputNetwork.VertexCount)
                {
                    //Если произошло уменьшение числа вершин, то просим подтверждение операции пользователем 

                    string message = "При уменьшении количества вершин в сети, ребра соединяющие отсутствующие вершины будут удалены.\n" +
                                    "Продолжить изменения?";

                    DialogResult dialogResult = MessageBox.Show(message, "Предупреждение", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                    //Если пользователь согласен уменьшить число вершин, то удалям отсутсвующие вершины
                    //и ребра, которые их содержат и обновляем выпадающие списки таблицы ребер
                    if (dialogResult == DialogResult.Yes)
                    {
                        inputNetwork.VertexCount = newVertexCount;

                        for (int i = 0; i < inputNetwork.Edges.Count; i++)
                        {
                            if (inputNetwork.Edges[i].FirstVertex >= newVertexCount || inputNetwork.Edges[i].SecondVertex >= newVertexCount)
                            {
                                inputNetwork.Edges.RemoveAt(i);
                                i--;
                            }
                        }
                        ChangeItemsComboBoxColumnsOfDataGridViewТаблицаРебер();
                    }
                }
                else
                {
                    //Если произошло увеличение числа вершин, то обновляем выпадающие списки таблицы ребер

                    inputNetwork.VertexCount = newVertexCount;


                    ChangeItemsComboBoxColumnsOfDataGridViewТаблицаРебер();
                }

                labelЧислоВершин.Text = "Число вершин:" + inputNetwork.VertexCount;

                //Обновляем выпадающие списки значений истока и стока
                ChangeItemsComboBoxesИстокAndСток();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, PROGRAM_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Добавляем новое ребро в сеть
        /// </summary>
        private void buttonДобавитьРебро_Click(object sender, EventArgs e)
        {
            if (inputNetwork.VertexCount > 0)
            {
                inputNetwork.Edges.Add(new InputEdge());
            }
        }

        private void buttonОбновитьГраф_Click(object sender, EventArgs e)
        {
            seacher = null;

            graphController.Graph = new Graph(inputNetwork, pictureBoxГраф.Width, pictureBoxГраф.Height, 25);
            graphController.PlaceGraph(true);

            richTextBoxРезультат.Clear();
            richTextBoxХодРешения.Clear();

            pictureBoxГраф.Invalidate();
        }

        private void buttonУдалитьРешение_Click(object sender, EventArgs e)
        {
            if (seacher != null)
            {
                seacher = null;
                graphController.Graph = new Graph(inputNetwork, pictureBoxГраф.Width, pictureBoxГраф.Height, 25);
                graphController.PlaceGraph(true);

                richTextBoxХодРешения.Clear();
                richTextBoxРезультат.Clear();

                pictureBoxГраф.Invalidate();
            }
        }

        /// <summary>
        /// Очищаем сеть
        /// </summary>
        private void buttonУдалитьСеть_Click(object sender, EventArgs e)
        {
            inputNetwork.Clear();

            sourceValues.Clear();
            targetValues.Clear();

            seacher = null;

            labelЧислоВершин.Text = "Число вершин:не задано";
            textBoxЧислоВершин.Clear();

            graphController.ResetLastLocation();
            graphController.Graph = null;

            richTextBoxХодРешения.Clear();
            richTextBoxРезультат.Clear();

            pictureBoxГраф.Invalidate();
        }

        /// <summary>
        /// Задаем вершину истока в сети
        /// </summary>
        private void comboBoxИсток_SelectedIndexChanged(object sender, EventArgs e)
        {
            inputNetwork.Source = comboBoxИсток.SelectedIndex;
        }

        /// <summary>
        /// Задаем вершину стока в сети
        /// </summary>
        private void comboBoxСток_SelectedIndexChanged(object sender, EventArgs e)
        {
            inputNetwork.Target = comboBoxСток.SelectedIndex;
        }

        private void comboBoxИсток_DataSourceChanged(object sender, EventArgs e)
        {
            if (comboBoxИсток.DataSource != null)
            {
                comboBoxИсток.SelectedIndex = 0;
            }
        }

        private void comboBoxСток_DataSourceChanged(object sender, EventArgs e)
        {
            if (comboBoxСток.DataSource != null)
            {
                comboBoxСток.SelectedIndex = 0;
            }
        }

        private void dataGridViewТаблицаРебер_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                TextBox textBoxПропускнаяСпособность = (TextBox)e.Control;

                textBoxПропускнаяСпособность.KeyPress += new KeyPressEventHandler
                (
                    (par_sender, par_e) =>
                    {
                        if (!char.IsDigit(par_e.KeyChar) && par_e.KeyChar != (char)Keys.Back)
                        {
                            par_e.Handled = true;
                        }
                    }
                );
            }
        }

        private void pictureBoxГраф_Paint(object sender, PaintEventArgs e)
        {
            if (graphController.IsSetGraph)
            {
                if (seacher != null)
                {
                    if (radioButtonКонечноеРешение.Checked)
                    {
                        graphController.PaintGraph(e.Graphics);
                    }
                    if (radioButtonХодРешения.Checked)
                    {
                        graphController.PaintGraph(e.Graphics, seacher);
                    }
                }
                else
                {
                    graphController.PaintGraph(e.Graphics);
                }
            }
            else
            {
                e.Graphics.Clear(SystemColors.Window);
            }
        }

        private void pictureBoxГраф_MouseDown(object sender, MouseEventArgs e)
        {
            if (graphController.IsSetGraph && e.Button == MouseButtons.Left)
            {
                graphController.SetSelectVertex(e.Location);
            }
        }

        private void pictureBoxГраф_MouseUp(object sender, MouseEventArgs e)
        {
            if (graphController.IsSetGraph)
            {
                graphController.ResetSelectVertex();
            }
        }

        private void pictureBoxГраф_MouseMove(object sender, MouseEventArgs e)
        {
            if (graphController.IsSetGraph)
            {
                Cursor = graphController.ChangeCursorOverVertex(e.Location);

                if (e.Button == MouseButtons.Left)
                {
                    graphController.ChangeLocationSelectVertex(e.Location);
                    pictureBoxГраф.Invalidate();
                }
            }
        }

        private void textBoxЧислоВершин_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Изменяем выпадающие списки таблицы ребер при изменении количества вершин в сети
        /// </summary>
        private void ChangeItemsComboBoxColumnsOfDataGridViewТаблицаРебер()
        {
            int[] vertices = new int[inputNetwork.VertexCount];
            for (int i = 0; i < inputNetwork.VertexCount; i++)
            {
                vertices[i] = i;
            }

            DataGridViewComboBoxColumn columnПерваяВершина = dataGridViewТаблицаРебер.Columns["ПерваяВершина"] as DataGridViewComboBoxColumn;
            columnПерваяВершина.DataSource = vertices;

            DataGridViewComboBoxColumn columnВтораяВершина = dataGridViewТаблицаРебер.Columns["ВтораяВершина"] as DataGridViewComboBoxColumn;
            columnВтораяВершина.DataSource = vertices;
        }

        /// <summary>
        /// Изменяем выпадающие списки истока и стока при изменении количества вершин в сети
        /// </summary>
        private void ChangeItemsComboBoxesИстокAndСток()
        {
            int comboBoxCount = sourceValues.Count;

            if (inputNetwork.Source > inputNetwork.VertexCount - 1)
            {
                comboBoxИсток.SelectedIndex = 0;
            }
            if (inputNetwork.Target > inputNetwork.VertexCount - 1)
            {
                comboBoxСток.SelectedIndex = 0;
            }

            //Если новое число вершин в сети больше старого числа вершин, 
            //добавляем новые вершины в выпадающий список
            if (comboBoxCount < inputNetwork.VertexCount)
            {
                for (int i = comboBoxCount; i < inputNetwork.VertexCount; i++)
                {
                    sourceValues.Add(i);
                    targetValues.Add(i);
                }
            }
            //Если новое число вершин в сети меньше старого числа вершин, 
            //удаляем отсутствующие вершины из выпадающего списка
            else if (comboBoxCount > inputNetwork.VertexCount)
            {
                //Удаляем отсутсвующие вершины из выпадающего списка
                for (int i = inputNetwork.VertexCount; i < comboBoxCount; i++)
                {
                    sourceValues.RemoveAt(sourceValues.Count - 1);
                    targetValues.RemoveAt(targetValues.Count - 1);
                }
            }
        }

        private void radioButtonХодРешения_CheckedChanged(object sender, EventArgs e)
        {
            pictureBoxГраф.Invalidate();
        }

        private void buttonСохранитьСеть_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "Text Files(*.txt) | *.txt";

            try
            {
                if (inputNetwork.Empty)
                {
                    throw new InputNetworkException("Сеть не задана");
                }

                if (saveDialog.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(saveDialog.FileName, inputNetwork.ToTextFile());

                    MessageBox.Show("Сеть сохранена", PROGRAM_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, PROGRAM_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
