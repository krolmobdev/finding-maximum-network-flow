﻿using System;
using System.Drawing;

namespace MaximumNetworkFlow
{
    static class GraphOperations
    {
        public const int START_VERTEX_ARC_ANGLE = 45;
        public const int SWEEP_VERTEX_ARC_ANGLE = 260;

        /// <summary>
        /// Находит точку касания ребра с конечной вершиной
        /// </summary>
        public static Point EdgeTouchPointWithEndVertex(Point startVertex, Point endVertex, int vertexRadius)
        {
            //Расстояние между центрами вершин
            double edgeLength = PointsSpaceCharacteristics.Distance(startVertex, endVertex);

            //Рассчитываем координаты точки касания
            double cosAlfa = (endVertex.X - startVertex.X) / edgeLength;
            double sinAlfa = (endVertex.Y - startVertex.Y) / edgeLength;

            Point touchPoint = Point.Empty;

            touchPoint.X = (int)(endVertex.X - vertexRadius * cosAlfa);
            touchPoint.Y = (int)(endVertex.Y - vertexRadius * sinAlfa);

            return touchPoint;
        }

        /// <summary>
        /// Находит сдвиг точки касания вершины на угол angle по окружности вершины
        /// </summary>
        /// <param name="vertexPoint">Вершина</param>
        /// <param name="touchPoint">Точка касания ребра с вершиной</param>
        /// <param name="angle">Угол</param>
        public static Point MoveVertexTouchPointOnAngle(Point vertexPoint, Point touchPoint, double angle)
        {
            //Преобразуем координаты точки касания к новой системе координат с началом в центре вершины
            int xn = touchPoint.X - vertexPoint.X;
            int yn = touchPoint.Y - vertexPoint.Y;

            //Вычисляем поворот точки на угол angle
            int x = (int)(xn * Math.Cos(angle) - yn * Math.Sin(angle));
            int y = (int)(xn * Math.Sin(angle) + yn * Math.Cos(angle));

            //Возвращаемся к старым координатам
            x += vertexPoint.X;
            y += vertexPoint.Y;

            return new Point(x, y);
        }

        /// <summary>
        /// Прямоугольник, который ограничивает вершину
        /// </summary>
        public static Rectangle VertexRectangle(Point vertexPoint, int vertexRadius)
        {
            return new Rectangle(vertexPoint.X - vertexRadius, vertexPoint.Y - vertexRadius, vertexRadius * 2, vertexRadius * 2);
        }


        /// <summary>
        /// Прямоугольник, который ограничивает дугу к вершине
        /// </summary>
        public static Rectangle VertexArcRectangle(Point vertexPoint, int vertexRadius)
        {
            return new Rectangle(vertexPoint.X - 2 * vertexRadius, vertexPoint.Y - vertexRadius, vertexRadius * 2, vertexRadius * 2);
        }

        /// <summary>
        /// Координаты средней точки арки вершины
        /// </summary>
        public static Point VertexArcMiddlePoint(Point vertexPoint, int vertexRadius)
        {
            return new Point(vertexPoint.X - 2 * vertexRadius, vertexPoint.Y);
        }

        /// <summary>
        /// Точка, делящая отрезок ребра в заданном отношении
        /// </summary>
        /// <param name="coeff">Коэффициент деления отрезка</param>
        public static PointF EdgePartPoint(PointF startPoint, PointF endPoint, float coeff)
        {
            if (coeff == -1)
            {
                throw new ArgumentOutOfRangeException(nameof(coeff));
            }

            float x = (startPoint.X + coeff * endPoint.X) / (1 + coeff);
            float y = (startPoint.Y + coeff * endPoint.Y) / (1 + coeff);

            return new PointF(x, y);
        }
    }
}
