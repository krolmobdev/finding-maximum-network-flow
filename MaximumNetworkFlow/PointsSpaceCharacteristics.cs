﻿using System;
using System.Drawing;

namespace MaximumNetworkFlow
{
    /// <summary>
    /// Пространственные характеристики точек
    /// </summary>
    static class PointsSpaceCharacteristics
    {
        /// <summary>
        /// Евклидово расстояние между 2-мя точками
        /// </summary>
        public static float Distance(PointF firstPoint, PointF secondPoint)
        {
            return Norm(Difference(firstPoint, secondPoint));
        }

        /// <summary>
        /// Нормированная разность двух точек
        /// </summary>
        /// <returns></returns>
        public static PointF NormDifference(PointF firstPoint, PointF secondPoint)
        {
            PointF diff = Difference(firstPoint, secondPoint);

            return new PointF(diff.X / Norm(diff), diff.Y / Norm(diff));
        }

        /// <summary>
        /// Возвращает норму точки в евклидовом пространстве
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public static float Norm(PointF point)
        {
            return (float)Math.Sqrt(Math.Pow(point.X, 2) + Math.Pow(point.Y, 2));
        }

        /// <summary>
        /// Возвращает разность двух точек
        /// </summary>
        public static PointF Difference(PointF firstPoint, PointF secondPoint)
        {
            return new PointF(secondPoint.X - firstPoint.X, secondPoint.Y - firstPoint.Y);
        }
    }
}
