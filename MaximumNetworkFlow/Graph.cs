﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace MaximumNetworkFlow
{
    class Graph
    {
        public const double EDGE_MOVE_ANGLE = Math.PI / 4;

        private HashSet<FlowEdge> edges = new HashSet<FlowEdge>();
        private List<Vertex> vertices = new List<Vertex>();
        private readonly int vertexCount;

        public int VertexCount
        {
            get
            {
                return vertexCount;
            }
        }

        public List<Vertex> Vertices
        {
            get
            {
                return vertices;
            }
        }

        public List<Point> VerticesLocation
        {
            get
            {
                List<Point> locVertices = new List<Point>(VertexCount);

                foreach (Vertex vertex in Vertices)
                {
                    locVertices.Add(vertex.VertexPoint);
                }

                return locVertices;
            }
        }

        /// <summary>
        /// Присваивает начальные значения вершинам из массива начальных значений
        /// Если размер location меньше числа вершин графа, то присваивает начальные значения locations.Count вершинам 
        /// Если размер location больше числа вершин графа, то присваивает начальные значения VertexCount вершинам 
        /// </summary>
        /// <param name="locations"></param>
        public void SetVerticesLocation(List<Point> locations)
        {
            if (locations == null)
            {
                throw new ArgumentNullException(nameof(locations));
            }

            int i = 0;
            foreach (Vertex vertex in vertices)
            {
                if (i == locations.Count)
                {
                    break;
                }

                vertex.VertexPoint = locations[i];
                i++;
            }
        }

        public HashSet<FlowEdge> Edges
        {
            get
            {
                return edges;
            }
        }

        public int AreaWidth { get; private set; }

        public int AreaHeight { get; private set; }

        public int Area
        {
            get
            {
                return AreaWidth * AreaHeight;
            }
        }

        /// <summary>
        /// Создает граф с начальным произвольным размением вершин 
        /// </summary>
        public Graph(InputNetwork network, int areaWidth, int areaHeight, int vertexRadius)
        {
            if (network.Empty)
            {
                return;
            }

            edges = new HashSet<FlowEdge>();

            foreach (InputEdge edge in network.Edges)
            {
                edges.Add(new FlowEdge(edge));
            }

            vertexCount = network.VertexCount;
            AreaWidth = areaWidth;
            AreaHeight = areaHeight;

            vertices = new List<Vertex>(vertexCount);
            for (int i = 0; i < VertexCount; i++)
            {
                Vertex vertex = new Vertex(i, vertexRadius);
                vertices.Add(vertex);
            }

            vertices[network.Source].IsSource = true;
            vertices[network.Target].IsTarget = true;

            RandomPlaceVertices();
        }

        private void RandomPlaceVertices()
        {
            int x = 0;
            int y = 0;

            Random rand = new Random();
            foreach (Vertex vertex in vertices)
            {
                if (vertex.IsSource)
                {

                    x = vertex.Diameter;
                    y = AreaHeight / 2;
                }
                else if (vertex.IsSource)
                {

                    x = AreaWidth - vertex.Diameter;
                    y = AreaHeight / 2;
                }
                else
                {
                    x = rand.Next(vertex.Diameter, AreaWidth - vertex.Diameter);
                    y = rand.Next(vertex.Diameter, AreaHeight - vertex.Diameter);
                }
                vertex.VertexPoint = new Point(x, y);
            }
        }

        /// <summary>
        /// Разместить вершины по силовому алгоритму Идеса
        /// </summary>
        /// <param name="iterCount">Число итераций</param>
        public void PlaceVertices(int iterCount)
        {
            for (int i = 0; i < iterCount; i++)
            {
                PlaceVertices();
            }
        }


        /// <summary>
        /// Разместить вершины графа по силовому алгоритму Идеса 
        /// </summary>
        private void PlaceVertices()
        {
            PointsForceCharacteristics force = new PointsForceCharacteristics(Area, VertexCount);

            //Для каждой вершины рассчитываем суммарную силу притяжения со смежными с ней вершинами 
            //и суммарную силу отталкивания с несмежными ей вершинами
            //Каждое ребро графа рассматривается как ненаправленное
            foreach (Vertex curVertex in vertices)
            {
                //Множество смежных вершин для вершины curVertex
                HashSet<Vertex> adjVertices = new HashSet<Vertex>();

                //Рассчитываем силу притяжения для curVertex со смежными ей вершинами
                foreach (FlowEdge edge in edges)
                {
                    //Если ребро edge начинается с вершины curVertex или заканчивается curVertex и еще не встречалось ребра с вершиной curVertex 
                    //(т.е множество смежных вершин adjVertices не содержит другую вершину adjVertex ребра), 
                    //то увеличиваем значение силы притяжения  для вершин ребра edge
                    if (edge.From == curVertex.Number && !adjVertices.Contains(vertices[edge.To]) || edge.To == curVertex.Number && !adjVertices.Contains(vertices[edge.From]))
                    {
                        //смежная вершина ребра
                        Vertex adjVertex = vertices[edge.OtherVertex(curVertex.Number)];

                        //Если данное ребро не было уже рассмотрено на предыдущей итерации алгоритма
                        if (adjVertex.Number > curVertex.Number)
                        {
                            //Увеличиваем силу притяжения для вершины curVertex, рассматривая прямое ребро (curVertex, adjVertex,)
                            PointF attractionForce = force.AttractionForce(curVertex.VertexPoint, adjVertex.VertexPoint);
                            curVertex.AddAttractionForce(attractionForce);

                            //Увеличиваем силу притяжения для вершины adjVertex, рассматривая обратное ребро (adjVertex, curVertex)
                            attractionForce = force.AttractionForce(adjVertex.VertexPoint, curVertex.VertexPoint);
                            adjVertex.AddAttractionForce(attractionForce);

                            //Добавляем вершину adjVertex в список смежных вершин - adjVertices вершины curVertex
                            adjVertices.Add(adjVertex);
                        }
                    }
                }

                //Рассчитываем силу отталкивания для curVertex с несмежными ей вершинами

                foreach (Vertex otherVertex in vertices)
                {
                    //Если другая вершина otherVertex не была уже рассмотрена на предыдущей интерации алгоритма
                    //и otherVertex не является смежной для текущей вершины curVertex
                    if (otherVertex.Number > curVertex.Number && !adjVertices.Contains(otherVertex))
                    {
                        PointF repulsiveForce = force.RepulsiveForce(curVertex.VertexPoint, otherVertex.VertexPoint);
                        curVertex.AddRepulsiveForce(repulsiveForce);

                        repulsiveForce = force.RepulsiveForce(otherVertex.VertexPoint, curVertex.VertexPoint);
                        otherVertex.AddRepulsiveForce(repulsiveForce);
                    }
                }
            }

            //Перемещаем вершины графа
            foreach (Vertex vertex in vertices)
            {
                vertex.ReplaceVertex(AreaWidth, AreaHeight);

                //Обнуляем значение сил притяжения и отталкивания 
                vertex.ResetAttractionForce();
                vertex.ResetRepulsiveForce();
            }
        }

        /// <summary>
        /// Имеется ли для ребра обратное ребро
        /// </summary>
        /// <param name="edge">Ребро</param>
        /// <exception cref="ArgumentNullException"></exception>
        public bool IsHaveReverseEdge(FlowEdge edge)
        {
            if (edge == null)
            {
                throw new ArgumentNullException(nameof(edge));
            }

            return edges.Contains(new FlowEdge(edge.To, edge.From, 0));
        }

        public Vertex EdgeStartVertex(FlowEdge edge)
        {
            if (edge == null)
            {
                throw new ArgumentNullException(nameof(edge));
            }

            if (!edges.Contains(edge))
            {
                throw new ArgumentException("Недопустимое ребро");
            }

            return vertices[edge.From];
        }

        public Vertex EdgeEndVertex(FlowEdge edge)
        {
            if (edge == null)
            {
                throw new ArgumentNullException(nameof(edge));
            }

            if (!edges.Contains(edge))
            {
                throw new ArgumentException("Недопустимое ребро");
            }

            return vertices[edge.To];
        }

        /// <summary>
        /// Возвращает начальную и конечную точки касания ребра с вершинами
        /// </summary>
        public Point[] EdgeTouchPoint(FlowEdge edge)
        {
            if (edge == null)
            {
                throw new ArgumentNullException(nameof(edge));
            }

            if (!edges.Contains(edge))
            {
                throw new ArgumentException("Недопустимое ребро");
            }

            Point[] touchPoints = new Point[2];

            Vertex startVertex = EdgeStartVertex(edge);
            Vertex endVertex = EdgeEndVertex(edge);

            touchPoints[0] = GraphOperations.EdgeTouchPointWithEndVertex(endVertex.VertexPoint, startVertex.VertexPoint, endVertex.Radius);
            touchPoints[1] = GraphOperations.EdgeTouchPointWithEndVertex(startVertex.VertexPoint, endVertex.VertexPoint, startVertex.Radius);

            return touchPoints;
        }

        /// <summary>
        /// Возвращает координаты точек касания ребра с вершинами, сдвинутого по окружности вершины на угол ANGLE
        /// </summary>
        public Point[] MoveEdgeTouchPoint(FlowEdge edge)
        {
            if (edge == null)
            {
                throw new ArgumentNullException(nameof(edge));
            }

            if (!edges.Contains(edge))
            {
                throw new ArgumentException("Недопустимое ребро");
            }

            Point[] touchPoints = EdgeTouchPoint(edge);

            Vertex startVertex = EdgeStartVertex(edge);
            Vertex endVertex = EdgeEndVertex(edge);

            touchPoints[0] = GraphOperations.MoveVertexTouchPointOnAngle(startVertex.VertexPoint, touchPoints[0], -EDGE_MOVE_ANGLE);
            touchPoints[1] = GraphOperations.MoveVertexTouchPointOnAngle(endVertex.VertexPoint, touchPoints[1], EDGE_MOVE_ANGLE);

            return touchPoints;
        }

        /// <summary>
        /// Возвращает прямоугольник, ограничивающий петлю ребра-петли, у которого совпадает исток и сток
        /// </summary>
        /// <returns></returns>
        public Rectangle EdgeArcRectangle(FlowEdge edge)
        {
            if (edge == null)
            {
                throw new ArgumentNullException(nameof(edge));
            }

            if (!edges.Contains(edge))
            {
                throw new ArgumentException("Недопустимое ребро");
            }
            if (edge.To == edge.Flow)
            {
                throw new ArgumentException("Ребро не является петлей");
            }

            return vertices[edge.From].VertexArc;
        }

        /// <summary>
        /// Возвращает вершину по координатам точки или
        /// null, если координаты точки не удовлетворяют ни одной из вершин графа
        /// </summary>
        /// <param name="point">Координаты точки</param>
        public Vertex SelectVertex(Point point)
        {
            Vertex selectVertex = null;

            if ((point.X >= 0 && point.X <= AreaWidth) &&
                (point.Y >= 0 && point.Y <= AreaHeight))
            {
                foreach (Vertex vertex in vertices)
                {
                    int distance = (int)PointsSpaceCharacteristics.Distance(vertex.VertexPoint, point);

                    if (distance < vertex.Radius)
                    {
                        selectVertex = vertex;
                        break;
                    }
                }
            }
            return selectVertex;
        }
    }
}
