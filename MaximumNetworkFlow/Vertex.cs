﻿using System.Drawing;

namespace MaximumNetworkFlow
{
    class Vertex
    {
        public const int START_ARC_ANGLE = GraphOperations.START_VERTEX_ARC_ANGLE;
        public const int SWEEP_ARC_ANGLE = GraphOperations.SWEEP_VERTEX_ARC_ANGLE;

        /// <summary>
        /// Номер вершины
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Радиус вершины
        /// </summary>
        public int Radius { get; set; }

        /// <summary>
        /// Диаметр вершины
        /// </summary>
        public int Diameter
        {
            get
            {
                return Radius * 2;
            }
        }

        /// <summary>
        /// Является ли вершина источником
        /// </summary>
        public bool IsSource { get; set; }

        /// <summary>
        /// Является ли вершина стоком
        /// </summary>
        public bool IsTarget { get; set; }

        /// <summary>
        /// Координаты вершины
        /// </summary>
        public Point VertexPoint { get; set; }

        /// <summary>
        /// Сила притяжения вершины относительно других вершин
        /// </summary>
        public PointF AttractionForce { get; private set; }

        /// <summary>
        /// Сила отталкивания относительно других вершин
        /// </summary>
        public PointF RepulsiveForce { get; private set; }

        /// <summary>
        /// Равнодействующая сила: сумма силы притяжения+силы отталкаивания
        /// </summary>
        public PointF ResulantForce
        {
            get
            {
                PointF ResulantForce = new PointF(AttractionForce.X + RepulsiveForce.X, AttractionForce.Y + RepulsiveForce.Y);

                return ResulantForce;
            }
        }

        /// <summary>
        /// Прямоугольник, ограничивающий вершину
        /// </summary>
        public Rectangle VertexRectangle
        {
            get
            {
                return GraphOperations.VertexRectangle(VertexPoint, Radius);
            }
        }

        /// <summary>
        /// Прямоугольник, ограничивающий арку вершины
        /// </summary>
        public Rectangle VertexArc
        {
            get
            {
                return GraphOperations.VertexArcRectangle(VertexPoint, Radius);
            }
        }

        public Point VertexArcMiddlePoint
        {
            get
            {
                return GraphOperations.VertexArcMiddlePoint(VertexPoint, Radius);
            }
        }

        public Vertex(int number, int radius)
        {
            Number = number;
            VertexPoint = Point.Empty;
            Radius = radius;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Vertex vertex = (Vertex)obj;

            return (Number == vertex.Number) && (VertexPoint == vertex.VertexPoint) && (Radius == vertex.Radius)
                && (AttractionForce == vertex.AttractionForce) && (RepulsiveForce == vertex.RepulsiveForce);
        }

        public override int GetHashCode()
        {
            int result = 17;
            result = 31 * result + Number;
            result = 31 * result + VertexPoint.GetHashCode();
            result = 31 * result + (int)Radius;
            result = 31 * result + AttractionForce.GetHashCode();
            result = 31 * result + RepulsiveForce.GetHashCode();

            return result;
        }

        /// <summary>
        /// Добавить силу притяжения без перемещения вершины
        /// </summary>
        /// <param name="force"></param>
        public void AddAttractionForce(PointF force)
        {
            AttractionForce = new PointF(AttractionForce.X + force.X, AttractionForce.Y + force.Y);
        }

        /// <summary>
        /// Обнулить значение силы притяжения без перемещения вершины
        /// </summary>
        public void ResetAttractionForce()
        {
            AttractionForce = PointF.Empty;
        }

        /// <summary>
        /// Добавить значение силы отталкивания
        /// </summary>
        /// <param name="force"></param>
        public void AddRepulsiveForce(PointF force)
        {
            RepulsiveForce = new PointF(RepulsiveForce.X + force.X, RepulsiveForce.Y + force.Y);
        }

        /// <summary>
        /// Сбросить значение силы отталкивания
        /// </summary>
        public void ResetRepulsiveForce()
        {
            RepulsiveForce = PointF.Empty;
        }

        /// <summary>
        /// Переместить вершину на величину равнодействующей силы
        /// </summary>
        public void ReplaceVertex(int areaWidth, int areaHeight)
        {
            Point offset = Point.Ceiling(new PointF(PointsForceCharacteristics.DELTA * ResulantForce.X, PointsForceCharacteristics.DELTA * ResulantForce.Y));

            int new_x = VertexPoint.X + offset.X;
            int new_y = VertexPoint.Y + offset.Y;

            if ((new_x <= areaWidth - Diameter && new_x > Diameter) && (new_y <= areaHeight - Diameter && new_y > Diameter))
            {
                VertexPoint = new Point(new_x, new_y);
            }
        }
    }
}
