﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace MaximumNetworkFlow
{
    /// <summary>
    /// Описывает сеть, которую вводит пользователь
    /// </summary>
    class InputNetwork
    {
        /// <summary>
        /// Список ребер
        /// </summary>
        private BindingList<InputEdge> edges = new BindingList<InputEdge>();

        /// <summary>
        /// Список ребер сети
        /// </summary>
        public BindingList<InputEdge> Edges
        {
            get
            {
                return edges;
            }
        }

        /// <summary>
        /// Число вершин
        /// </summary>
        public int VertexCount { get; set; }

        /// <summary>
        /// Источник
        /// </summary>
        public int Source { get; set; }

        /// <summary>
        /// Сток
        /// </summary>
        public int Target { get; set; }

        public bool Empty
        {
            get
            {
                return VertexCount == 0;
            }
        }

        public InputNetwork() { }

        public InputNetwork(FlowNetwork network)
        {
            edges = new BindingList<InputEdge>();

            foreach (FlowEdge edge in network.Edges)
            {
                edges.Add(new InputEdge(edge));
            }

            VertexCount = network.VertexCount;
            Source = network.Source;
            Target = network.Target;
        }

        /// <summary>
        /// Создание потоковой сети из данных текстового файла.
        /// Формат данных: 
        /// 1 строка: число вершин, 
        /// 2 строка: исток,
        /// 3 стока: сток
        /// остальные строки - ребра в формате: firstVertex secondVertex capacity
        /// </summary>
        /// <param name="parsingData">Данные текстового файла</param>
        /// <exception cref="ArgumentException"></exception>
        public InputNetwork(string[] parsingData)
        {
            try
            {
                List<string> errorMessages = new List<string>();

                VertexCount = int.Parse(parsingData[0]);


                if (VertexCount == 0)
                {
                    throw new ArgumentException("Число вершин равно нулю");
                }

                Source = int.Parse(parsingData[1]);
                Target = int.Parse(parsingData[2]);


                if (Source > VertexCount - 1)
                {
                    errorMessages.Add("Вершина истока " + Source + " отсутствует в сети.");
                }
                if (Target > VertexCount - 1)
                {
                    errorMessages.Add("Вершина стока " + Target + " отсутствует в сети.");
                }

                for (int i = 3; i < parsingData.Length; i++)
                {
                    string[] edgeParameters = parsingData[i].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    int firstVertex = int.Parse(edgeParameters[0]);
                    int secondVertex = int.Parse(edgeParameters[1]);
                    int capacity = int.Parse(edgeParameters[2]);


                    if (firstVertex > VertexCount - 1 || secondVertex > VertexCount - 1)
                    {
                        errorMessages.Add("Ребро " + firstVertex + "->" + secondVertex + " содержит вершины, которые отсутствуют в сети.");
                    }

                    if (capacity < 0)
                    {
                        errorMessages.Add("Пропускная способность ребра  " + firstVertex + "->" + secondVertex + " меньше нуля.");
                    }
                    edges.Add(new InputEdge(firstVertex, secondVertex, capacity));
                }

                if (errorMessages.Count != 0)
                {
                    string errorMessage = "";

                    foreach (string item in errorMessages)
                    {
                        errorMessage += item + "\n";
                    }

                    throw new ArgumentException(errorMessage);
                }
            }
            catch (ArgumentException ex)
            {
                throw new ArgumentException("Входные данные сети имеют недопустимый формат:\n" + ex.Message);
            }
            catch (Exception)
            {
                throw new ArgumentException("Входные данные сети имеют недопустимый формат");
            }
        }

        public void Clear()
        {
            edges.Clear();
            Source = 0;
            Target = 0;
            VertexCount = 0;
        }

        /// <summary>
        /// Равенство вершин истока и стока
        /// </summary>
        public bool IsEqualsOfSourceAndTarget()
        {
            return Source == Target;
        }

        public override string ToString()
        {
            string result = "";

            result += VertexCount + "\n";
            result += Source + "\n";
            result += Target + "\n";

            foreach (InputEdge edge in Edges)
            {
                result += edge + "\n";
            }

            return result;
        }

        public string ToTextFile()
        {
            string result = "";

            result += VertexCount + "\r\n";
            result += Source + "\r\n";
            result += Target + "\r\n";

            foreach (InputEdge edge in Edges)
            {
                result += edge.ToTextFile() + "\r\n";
            }

            return result;
        }
    }
}
