﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace MaximumNetworkFlow
{
    class GraphController
    {
        public const int EDGE_FONT_SIZE = 9;
        public const int EDGE_LINE_SIZE = 3;
        public const int VERTEX_FONT_SIZE = 12;

        private List<Point> verticesLocation;

        private Graph graph;

        public Graph Graph
        {
            get
            {
                return graph;
            }

            set
            {
                SelectVertex = null;
                graph = value;
            }
        }

        public bool IsSetGraph
        {
            get
            {
                return Graph != null;
            }
        }

        public Vertex SelectVertex { get; private set; }


        public GraphController()
        {
        }

        public GraphController(Graph graph)
        {
            this.graph = graph;
        }

        public void PlaceGraph(bool isRestoreLastLocation)
        {
            Graph.PlaceVertices(10);

            if (verticesLocation != null && isRestoreLastLocation)
            {
                Graph.SetVerticesLocation(verticesLocation);
            }
            verticesLocation = Graph.VerticesLocation;
        }


        public void ResetLastLocation()
        {
            verticesLocation = null;
        }


        public void PaintGraph(Graphics graphics)
        {
            if (Graph == null)
            {
                throw new NullReferenceException("Свойство Graph равно null");
            }

            graphics.SmoothingMode = SmoothingMode.AntiAlias;
            graphics.Clear(SystemColors.Window);
            PaintGraphEdges(graphics);
            PainGraphVertices(graphics);
        }

        /// <summary>
        /// Рисует сеть c указанием пошагового значения потока
        /// </summary>
        /// <param name="alrorithm">Объект алгоритма поика потока, использовавшийся для нахождения максимального потока</param>
        public void PaintGraph(Graphics graphics, FordFalkersonAlgorithm alrorithm)
        {
            if (Graph == null)
            {
                throw new NullReferenceException("Свойство Graph равно null");
            }

            graphics.SmoothingMode = SmoothingMode.AntiAlias;
            graphics.Clear(SystemColors.Window);
            PaintGraphEdges(graphics, alrorithm);
            PainGraphVertices(graphics);
        }

        /// <summary>
        /// Рисует ребра и надписи над ними
        /// </summary>
        /// <param name="algorithm">Объект-алгоритм поиска максимального потока</param>
        private void PaintGraphEdges(Graphics graphics, FordFalkersonAlgorithm algorithm = null)
        {
            //Рисуем линию ребра

            Pen edgePen = new Pen(Color.Black, EDGE_LINE_SIZE);
            edgePen.EndCap = LineCap.ArrowAnchor;
            Font edgeFont = new Font("Tahoma", EDGE_FONT_SIZE, FontStyle.Bold);


            //Получение ребер, которые входят в минамальный разрез
            List<FlowEdge> cutEdges = new List<FlowEdge>();
            if (algorithm != null)
            {
                cutEdges = algorithm.MinCut;
            }

            //Множество содержит уже нарисованные ребра графа
            HashSet<FlowEdge> paintedEdges = new HashSet<FlowEdge>();

            foreach (FlowEdge edge in Graph.Edges)
            {
                //Изменяем цвет ребра, которое входит в разрез
                if (cutEdges.Contains(edge))
                {
                    edgePen.Color = Color.Blue;
                }
                else
                {
                    edgePen.Color = Color.Black;
                }

                string edgeLabel = "";

                //Получаем данные потока ребра на разных итерациях алгоритма
                if (algorithm != null)
                {
                    List<double> edgeFlows = algorithm.GetEdgeStepFlow(edge);

                    int count = edgeFlows.Count;
                    for (int i = 0; i < count; i++)
                    {
                        edgeLabel += edgeFlows[i];

                        if (i != count - 1)
                        {
                            edgeLabel += ", ";
                        }
                    }
                }
                else//Выводим только начальное нулевое значение потока
                {
                    edgeLabel += edge.Flow;
                }


                // Находим точки касания ребра с вершинами
                Point[] touchPoints;

                //Если ребро не является петлей
                if (edge.To != edge.From)
                {
                    //Ребро edge недвунаправленное или не нарисована ни одна из его частей-однонаправленных ребер
                    if (!paintedEdges.Contains(edge.ReverseEdge))
                    {
                        touchPoints = Graph.EdgeTouchPoint(edge);
                    }
                    else //Одна часть-однонаправленное ребро двунаправленного ребра edge уже нарисована
                    {
                        touchPoints = Graph.MoveEdgeTouchPoint(edge);
                    }
                    graphics.DrawLine(edgePen, touchPoints[0], touchPoints[1]);


                    //Рисуем текст ребра

                    //Определяем начальную и конечную точку надписи ребра
                    PointF startPoint = new PointF(touchPoints[0].X, touchPoints[0].Y);
                    PointF endPoint = new PointF(touchPoints[1].X, touchPoints[1].Y);

                    //Если начальная точка надписи, находится левее конечной, 
                    //меняем их местами для правильного направления надписи
                    if (startPoint.X > endPoint.X)
                    {
                        PointF temp = startPoint;
                        startPoint = endPoint;
                        endPoint = temp;
                    }
                    //Смещаем начальную точку надписи на 1/5 часть длины ребра
                    startPoint = GraphOperations.EdgePartPoint(startPoint, endPoint, 0.16F);
                    //Выводим надпись ребра
                    GraphicsTextOperations.DrawTextOnSegment(graphics, Brushes.Blue, edgeFont, "c:" + edge.Capacity + " f:" + edgeLabel, 0, startPoint, endPoint, true);
                }
                else//Если ребро является петлей
                {
                    //Рисуем арку-ребро
                    graphics.DrawArc(edgePen, Graph.EdgeStartVertex(edge).VertexArc, Vertex.START_ARC_ANGLE, Vertex.SWEEP_ARC_ANGLE);

                    //Рисуем текст над аркой

                    StringFormat sf = new StringFormat(StringFormatFlags.DirectionRightToLeft);
                    graphics.DrawString("c:" + edge.Capacity + " f:" + edgeLabel, edgeFont, Brushes.Blue, Graph.EdgeStartVertex(edge).VertexArcMiddlePoint, sf);

                }
                //Добавляем ребро в множество уже нарисованных ребер
                paintedEdges.Add(edge);
            }
        }


        private void PainGraphVertices(Graphics graphics)
        {
            foreach (Vertex vertex in Graph.Vertices)
            {
                //Рисуем вершину

                graphics.DrawEllipse(Pens.Black, vertex.VertexRectangle);

                if (vertex.IsSource || vertex.IsTarget)
                {
                    graphics.FillEllipse(Brushes.Orange, vertex.VertexRectangle);
                }
                else
                {
                    graphics.FillEllipse(Brushes.Yellow, vertex.VertexRectangle);
                }

                //Рисуем текст вершины

                Font vertexFont = new Font("Tahoma", VERTEX_FONT_SIZE, FontStyle.Bold);
                StringFormat strFormat = new StringFormat();
                strFormat.Alignment = StringAlignment.Center;
                strFormat.LineAlignment = StringAlignment.Center;

                graphics.DrawString(vertex.Number.ToString(), vertexFont, Brushes.Black, vertex.VertexPoint, strFormat);
            }
        }

        public void SetSelectVertex(Point point)
        {
            if (Graph == null)
            {
                throw new NullReferenceException("Свойство Graph равно null");
            }

            SelectVertex = Graph.SelectVertex(point);
        }

        public void ResetSelectVertex()
        {
            if (Graph == null)
            {
                throw new NullReferenceException("Свойство Graph равно null");
            }

            SelectVertex = null;
        }

        public Cursor ChangeCursorOverVertex(Point point)
        {
            if (Graph == null)
            {
                throw new NullReferenceException("Свойство Graph равно null");
            }

            if (Graph.SelectVertex(point) != null)
            {
                return Cursors.Hand;
            }
            else
            {
                return Cursors.Default;
            }
        }

        public bool ChangeLocationSelectVertex(Point point)
        {
            if (Graph == null)
            {
                throw new NullReferenceException("Свойство Graph равно null");
            }

            bool isChanged = false;

            if (SelectVertex != null)
            {
                int vertexRadius = SelectVertex.Radius;

                if ((point.X >= vertexRadius && point.X <= Graph.AreaWidth - vertexRadius) &&
                    (point.Y >= vertexRadius && point.Y <= Graph.AreaHeight - vertexRadius))
                {
                    SelectVertex.VertexPoint = point;

                    verticesLocation = Graph.VerticesLocation;
                    isChanged = true;
                }
            }
            return isChanged;
        }
    }
}
