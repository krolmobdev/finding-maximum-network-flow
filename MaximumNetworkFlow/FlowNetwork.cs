﻿using System.Collections.Generic;

namespace MaximumNetworkFlow
{
    /// <summary>
    /// Описывает потоковую сеть c дублированием направленных ребер в обоих направлениях
    /// Для реализации сети используется модель неориентированного графа 
    /// c пропускными способностями (весами) и потоками.
    /// Данная модель позволяет дублировать ребра сети в обратном направлении, 
    /// что используется для нахождения максимального потока в сети
    /// </summary>
    class FlowNetwork
    {
        /// <summary>
        /// Число вершин
        /// </summary>
        private readonly int vertexCount;

        /// <summary>
        /// Число вершин в сети
        /// </summary>
        public int VertexCount
        {
            get
            {
                return vertexCount;
            }
        }

        /// <summary>
        /// Число ребер в сети
        /// </summary>
        public int EdgeCount { get; private set; }


        /// <summary>
        /// Массив списков смежных ребер каждой вершины сети
        /// </summary>
        private List<FlowEdge>[] adjacentEdges;


        /// <summary>
        /// Исток
        /// </summary>
        public int Source { get; private set; }

        /// <summary>
        /// Сток
        /// </summary>
        public int Target { get; private set; }


        /// <summary>
        /// Создание потоковой сети с числом вершин vertexCount и без ребер
        /// </summary>
        /// <param name="vertexCount">Число вершин</param>
        public FlowNetwork(int vertexCount, int source, int target)
        {
            this.vertexCount = vertexCount;
            Source = source;
            Target = target;

            //Создание массива списков смежных ребер каждой вершины сети
            //Вначале все списки создаются пустыми
            adjacentEdges = new List<FlowEdge>[vertexCount];
            for (int i = 0; i < vertexCount; i++)
            {
                adjacentEdges[i] = new List<FlowEdge>();
            }
        }

        /// <summary>
        /// Создание потоковой сети на основе сети inputNetwork
        /// </summary>
        public FlowNetwork(InputNetwork inputNetwork)
            : this(inputNetwork.VertexCount, inputNetwork.Source, inputNetwork.Target)
        {
            foreach (InputEdge edge in inputNetwork.Edges)
            {
                AddEdge(new FlowEdge(edge));
            }
        }

        /// <summary>
        /// Возвращает копию сети
        /// </summary>
        public FlowNetwork Copy
        {
            get
            {
                FlowNetwork network = new FlowNetwork(VertexCount, Source, Target);
                List<FlowEdge> edges = new List<FlowEdge>(EdgeCount);

                foreach (FlowEdge edge in Edges)
                {
                    network.AddEdge(new FlowEdge(edge));
                }

                return network;
            }
        }

        /// <summary>
        /// Добавление ребра e в потоковую сеть
        /// </summary>
        /// <param name="e">Ребро</param>
        public void AddEdge(FlowEdge e)
        {
            int firstVertex = e.From;
            int secondVertex = e.To;

            adjacentEdges[firstVertex].Add(e);
            adjacentEdges[secondVertex].Add(e);

            EdgeCount++;
        }

        /// <summary>
        /// Возвращает ребра, смежные с вершиной vertex
        /// </summary>
        /// <param name="vertex">Вершина</param>
        /// <returns></returns>
        public List<FlowEdge> AdjacentEdges(int vertex)
        {
            return adjacentEdges[vertex];
        }


        /// <summary>
        /// Возвращает все ребра потоковой сети без дублирующих ребер в обратном направлении
        /// </summary>
        /// <returns></returns>
        public List<FlowEdge> Edges
        {
            get
            {
                List<FlowEdge> edges = new List<FlowEdge>();

                for (int v = 0; v < vertexCount; v++)
                    foreach (FlowEdge e in adjacentEdges[v])
                    {
                        if (e.OtherVertex(v) > v)
                            edges.Add(e);
                    }
                return edges;
            }
        }

        public override string ToString()
        {
            string networkFlowData = "Число вершин:" + VertexCount;

            List<FlowEdge> edges = Edges;
            networkFlowData += "\nЧисло ребер:" + edges.Count;

            foreach (FlowEdge edge in edges)
            {
                networkFlowData += "\n" + edge;
            }

            return networkFlowData;
        }
    }
}
