﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaximumNetworkFlow
{
    /// <summary>
    /// Описывает ребро сети с заданным потоком
    /// </summary>
    class FlowEdge
    {
        /// <summary>
        /// Первая вершина ребра
        /// </summary>
        private readonly int firstVertex;

        /// <summary>
        /// Вторая вершина ребра
        /// </summary>
        private readonly int secondVertex;

        /// <summary>
        /// Вершина, из которой исходит данное ребро
        /// </summary>
        public int From
        {
            get
            {
                return firstVertex;
            }
        }

        /// <summary>
        /// Вершина, в которую входит данное ребро
        /// </summary>
        public int To
        {
            get
            {
                return secondVertex;
            }
        }


        /// <summary>
        /// Пропускная способность ребра
        /// </summary>
        public double Capacity { get; private set; }

        /// <summary>
        /// Поток в данном ребре
        /// </summary>
        /// <returns></returns>
        public double Flow { get; private set; }


        /// <summary>
        /// Создание ребра
        /// </summary>
        /// <param name="firstVertex">Первая вершина</param>
        /// <param name="secondVertex">Вторая вершина</param>
        /// <param name="capacity">Пропускная способность ребра</param>
        public FlowEdge(int firstVertex, int secondVertex, double capacity)
        {
            this.firstVertex = firstVertex;
            this.secondVertex = secondVertex;
            Capacity = capacity;
        }

        public FlowEdge(FlowEdge edge)
        {
            firstVertex = edge.From;
            secondVertex = edge.To;
            Capacity = edge.Capacity;
            Flow = edge.Flow;
        }

        /// <summary>
        /// Создание ребра на основе введенных данных
        /// </summary>
        /// <param name="edge"></param>
        public FlowEdge(InputEdge edge)
        {
            firstVertex = edge.FirstVertex;
            secondVertex = edge.SecondVertex;
            Capacity = edge.Capacity;
            Flow = edge.Flow;
        }

        /// <summary>
        /// Возвращает обратное ребро для данного с имеющейся пропускной способностью 
        /// </summary>
        public FlowEdge ReverseEdge
        {
            get
            {
               return new FlowEdge(this.To, this.From, this.Capacity);
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            FlowEdge vertex = (FlowEdge)obj;

            return (From == vertex.From) && (To == vertex.To);
        }

        public override int GetHashCode()
        {
            int result = 17;
            result = 31 * result + firstVertex;
            result = 31 * result + secondVertex;
        
            return result;
        }


        /// <summary>
        /// Возвращает другую вершину ребра
        /// </summary>
        /// <param name="vertex">Одна из вершин ребра</param>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public int OtherVertex(int vertex)
        {
            if (vertex == firstVertex)
            {
                return secondVertex;
            }
            else if (vertex == secondVertex)
            {
                return firstVertex;
            }
            else
            {
                throw new ArgumentOutOfRangeException("Недопустимое ребро");
            }
        }


        /// <summary>
        /// Остаточная пропускная способность в направлении к вершине vertex
        /// </summary>
        /// <param name="vertex">Вершина</param>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public double ResidualCapacityTo(int vertex)
        {
            if (vertex == firstVertex)
            {
                return Flow;
            }
            else if (vertex == secondVertex)
            {
                return Capacity - Flow;
            }
            else
            {
                throw new ArgumentOutOfRangeException("Недопустмое ребро");
            }
        }

        /// <summary>
        /// Добавление потока delta в направлении к вершине vertex
        /// </summary>
        /// <param name="vertex">Вершина</param>
        /// <param name="delta">Поток</param>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public void AddResidualFlowTo(int vertex,double delta)
        {
            if(vertex==firstVertex)
            {
                Flow -= delta;
            }
            else if(vertex==secondVertex)
            {
                Flow += delta;
            }
            else
            {
                throw new ArgumentOutOfRangeException("Недопустимое ребро");
            }
        }

        public override string ToString()
        {
            return "Ребро:"+firstVertex +"->"+ secondVertex + " Пропускная способность:" + Capacity + " Поток:" +Flow;
        }
    }
}
