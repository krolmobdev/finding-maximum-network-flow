﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace MaximumNetworkFlow
{
    static class GraphicsTextOperations
    {
        // Рисует текст над сегментом.
        // Оставьте char_num, указывая на следующий символ, который нужно нарисовать.
        // Оставьте start_point, удерживая последнюю используемую точку.
        public static void DrawTextOnSegment(Graphics gr, Brush brush,
            Font font, string txt, int first_ch,
            PointF start_point, PointF end_point,
            bool text_above_segment)
        {
            float dx = end_point.X - start_point.X;
            float dy = end_point.Y - start_point.Y;
            float dist = (float)Math.Sqrt(dx * dx + dy * dy);
            dx /= dist;
            dy /= dist;

            // Посмотрите, сколько символов будет соответствовать.
            int last_ch = first_ch;
            while (last_ch < txt.Length)
            {
                string test_string =
                    txt.Substring(first_ch, last_ch - first_ch + 1);
                if (gr.MeasureString(test_string, font).Width > dist)
                {
                    // Это слишком много символов.
                    last_ch--;
                    break;
                }
                last_ch++;
            }
            if (last_ch < first_ch) return;
            if (last_ch >= txt.Length) last_ch = txt.Length - 1;
            string chars_that_fit =
                txt.Substring(first_ch, last_ch - first_ch + 1);

            // Поворачиваем и переводим, чтобы расположить символы.
            GraphicsState state = gr.Save();
            if (text_above_segment)
            {
                gr.TranslateTransform(0,
                    -gr.MeasureString(chars_that_fit, font).Height,
                    MatrixOrder.Append);
            }
            float angle = (float)(180 * Math.Atan2(dy, dx) / Math.PI);
            gr.RotateTransform(angle, MatrixOrder.Append);
            gr.TranslateTransform(start_point.X, start_point.Y,
                MatrixOrder.Append);

            // Рисуем подходящие символы.
            gr.DrawString(chars_that_fit, font, brush, 0, 0);

            // Восстановить сохраненное состояние.
            gr.Restore(state);

            // Обновление first_ch и start_point.
            first_ch = last_ch + 1;
            float text_width =
                gr.MeasureString(chars_that_fit, font).Width;
            start_point = new PointF(
                start_point.X + dx * text_width,
                start_point.Y + dy * text_width);
        }
    }
}
