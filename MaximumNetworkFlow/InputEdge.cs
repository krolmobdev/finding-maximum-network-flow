﻿namespace MaximumNetworkFlow
{
    /// <summary>
    /// Описывает данные ребра сети, добавляемого пользователем
    /// </summary>
    class InputEdge
    {
        public int FirstVertex { get; set; }

        public int SecondVertex { get; set; }

        public int Capacity { get; set; }

        public int Flow { get; set; }

        public InputEdge() { }

        public InputEdge(int firstVertex, int secondVertex, int capacity)
        {
            FirstVertex = firstVertex;
            SecondVertex = secondVertex;
            Capacity = capacity;
        }

        public InputEdge(FlowEdge edge)
        {
            FirstVertex = edge.From;
            SecondVertex = edge.To;
            Capacity = (int)edge.Capacity;
            Flow = (int)edge.Flow;
        }

        public override string ToString()
        {
            return FirstVertex + " " + SecondVertex + " " + Capacity + "" + Flow;
        }

        public string ToTextFile()
        {
            return FirstVertex + " " + SecondVertex + " " + Capacity;
        }
    }
}
