﻿using System;

namespace MaximumNetworkFlow
{
    /// <summary>
    /// Описывает исключение при заполнении сети
    /// </summary>
    public class InputNetworkException : Exception
    {
        public InputNetworkException() : base()
        {
        }

        public InputNetworkException(String message) : base(message)
        {
        }
    }
}
