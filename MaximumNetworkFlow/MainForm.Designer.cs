﻿namespace MaximumNetworkFlow
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonЗагрузить = new System.Windows.Forms.Button();
            this.ПропускнаяСпособность = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ВтораяВершина = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ПерваяВершина = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewТаблицаРебер = new System.Windows.Forms.DataGridView();
            this.Поток = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelРебраСети = new System.Windows.Forms.Label();
            this.textBoxЧислоВершин = new System.Windows.Forms.TextBox();
            this.buttonЗадатьЧислоВершин = new System.Windows.Forms.Button();
            this.buttonДобавитьРебро = new System.Windows.Forms.Button();
            this.labelЧислоВершин = new System.Windows.Forms.Label();
            this.labelСток2 = new System.Windows.Forms.Label();
            this.labelИсток = new System.Windows.Forms.Label();
            this.comboBoxИсток = new System.Windows.Forms.ComboBox();
            this.comboBoxСток = new System.Windows.Forms.ComboBox();
            this.buttonУдалитьСеть = new System.Windows.Forms.Button();
            this.tabControlВизуализация = new System.Windows.Forms.TabControl();
            this.tabPageСеть = new System.Windows.Forms.TabPage();
            this.buttonСохранитьСеть = new System.Windows.Forms.Button();
            this.buttonОбновитьГраф = new System.Windows.Forms.Button();
            this.tabPageМаксимальныйПоток = new System.Windows.Forms.TabPage();
            this.richTextBoxРезультат = new System.Windows.Forms.RichTextBox();
            this.labelРезультат = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelПоказатьНаГрафе = new System.Windows.Forms.Label();
            this.radioButtonКонечноеРешение = new System.Windows.Forms.RadioButton();
            this.radioButtonХодРешения = new System.Windows.Forms.RadioButton();
            this.buttonУдалитьРешение = new System.Windows.Forms.Button();
            this.labelХодРешения = new System.Windows.Forms.Label();
            this.buttonНайтиПоток = new System.Windows.Forms.Button();
            this.richTextBoxХодРешения = new System.Windows.Forms.RichTextBox();
            this.labelГрафСети = new System.Windows.Forms.Label();
            this.pictureBoxГраф = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewТаблицаРебер)).BeginInit();
            this.tabControlВизуализация.SuspendLayout();
            this.tabPageСеть.SuspendLayout();
            this.tabPageМаксимальныйПоток.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxГраф)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonЗагрузить
            // 
            this.buttonЗагрузить.Location = new System.Drawing.Point(6, 445);
            this.buttonЗагрузить.Name = "buttonЗагрузить";
            this.buttonЗагрузить.Size = new System.Drawing.Size(101, 23);
            this.buttonЗагрузить.TabIndex = 2;
            this.buttonЗагрузить.Text = "Загрузить сеть";
            this.buttonЗагрузить.UseVisualStyleBackColor = true;
            this.buttonЗагрузить.Click += new System.EventHandler(this.buttonЗагрузить_Click);
            // 
            // ПропускнаяСпособность
            // 
            this.ПропускнаяСпособность.HeaderText = "Пропускная способность";
            this.ПропускнаяСпособность.Name = "ПропускнаяСпособность";
            this.ПропускнаяСпособность.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ПропускнаяСпособность.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ВтораяВершина
            // 
            this.ВтораяВершина.HeaderText = "Вторая вершина";
            this.ВтораяВершина.Name = "ВтораяВершина";
            this.ВтораяВершина.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ПерваяВершина
            // 
            this.ПерваяВершина.HeaderText = "Первая вершина";
            this.ПерваяВершина.Name = "ПерваяВершина";
            this.ПерваяВершина.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewТаблицаРебер
            // 
            this.dataGridViewТаблицаРебер.AllowUserToAddRows = false;
            this.dataGridViewТаблицаРебер.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewТаблицаРебер.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ПерваяВершина,
            this.ВтораяВершина,
            this.ПропускнаяСпособность,
            this.Поток});
            this.dataGridViewТаблицаРебер.Location = new System.Drawing.Point(6, 204);
            this.dataGridViewТаблицаРебер.Name = "dataGridViewТаблицаРебер";
            this.dataGridViewТаблицаРебер.Size = new System.Drawing.Size(361, 194);
            this.dataGridViewТаблицаРебер.TabIndex = 5;
            this.dataGridViewТаблицаРебер.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridViewТаблицаРебер_EditingControlShowing);
            // 
            // Поток
            // 
            this.Поток.HeaderText = "Поток";
            this.Поток.Name = "Поток";
            this.Поток.Visible = false;
            // 
            // labelРебраСети
            // 
            this.labelРебраСети.AutoSize = true;
            this.labelРебраСети.Location = new System.Drawing.Point(7, 188);
            this.labelРебраСети.Name = "labelРебраСети";
            this.labelРебраСети.Size = new System.Drawing.Size(64, 13);
            this.labelРебраСети.TabIndex = 6;
            this.labelРебраСети.Text = "Ребра сети";
            // 
            // textBoxЧислоВершин
            // 
            this.textBoxЧислоВершин.Location = new System.Drawing.Point(7, 19);
            this.textBoxЧислоВершин.Name = "textBoxЧислоВершин";
            this.textBoxЧислоВершин.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxЧислоВершин.Size = new System.Drawing.Size(121, 20);
            this.textBoxЧислоВершин.TabIndex = 7;
            this.textBoxЧислоВершин.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxЧислоВершин_KeyPress);
            // 
            // buttonЗадатьЧислоВершин
            // 
            this.buttonЗадатьЧислоВершин.Location = new System.Drawing.Point(7, 45);
            this.buttonЗадатьЧислоВершин.Name = "buttonЗадатьЧислоВершин";
            this.buttonЗадатьЧислоВершин.Size = new System.Drawing.Size(75, 23);
            this.buttonЗадатьЧислоВершин.TabIndex = 9;
            this.buttonЗадатьЧислоВершин.Text = "Задать";
            this.buttonЗадатьЧислоВершин.UseVisualStyleBackColor = true;
            this.buttonЗадатьЧислоВершин.Click += new System.EventHandler(this.buttonЗадатьЧислоВершин_Click);
            // 
            // buttonДобавитьРебро
            // 
            this.buttonДобавитьРебро.Enabled = false;
            this.buttonДобавитьРебро.Location = new System.Drawing.Point(6, 404);
            this.buttonДобавитьРебро.Name = "buttonДобавитьРебро";
            this.buttonДобавитьРебро.Size = new System.Drawing.Size(101, 23);
            this.buttonДобавитьРебро.TabIndex = 10;
            this.buttonДобавитьРебро.Text = "Добавить ребро";
            this.buttonДобавитьРебро.UseVisualStyleBackColor = true;
            this.buttonДобавитьРебро.Click += new System.EventHandler(this.buttonДобавитьРебро_Click);
            // 
            // labelЧислоВершин
            // 
            this.labelЧислоВершин.AutoSize = true;
            this.labelЧислоВершин.Location = new System.Drawing.Point(4, 3);
            this.labelЧислоВершин.Name = "labelЧислоВершин";
            this.labelЧислоВершин.Size = new System.Drawing.Size(137, 13);
            this.labelЧислоВершин.TabIndex = 11;
            this.labelЧислоВершин.Text = "Число вершин: не задано";
            // 
            // labelСток2
            // 
            this.labelСток2.AutoSize = true;
            this.labelСток2.Location = new System.Drawing.Point(5, 128);
            this.labelСток2.Name = "labelСток2";
            this.labelСток2.Size = new System.Drawing.Size(31, 13);
            this.labelСток2.TabIndex = 15;
            this.labelСток2.Text = "Сток";
            // 
            // labelИсток
            // 
            this.labelИсток.AutoSize = true;
            this.labelИсток.Location = new System.Drawing.Point(4, 88);
            this.labelИсток.Name = "labelИсток";
            this.labelИсток.Size = new System.Drawing.Size(38, 13);
            this.labelИсток.TabIndex = 19;
            this.labelИсток.Text = "Исток";
            // 
            // comboBoxИсток
            // 
            this.comboBoxИсток.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxИсток.FormattingEnabled = true;
            this.comboBoxИсток.Location = new System.Drawing.Point(7, 104);
            this.comboBoxИсток.Name = "comboBoxИсток";
            this.comboBoxИсток.Size = new System.Drawing.Size(121, 21);
            this.comboBoxИсток.TabIndex = 20;
            this.comboBoxИсток.SelectedIndexChanged += new System.EventHandler(this.comboBoxИсток_SelectedIndexChanged);
            this.comboBoxИсток.DataSourceChanged += new System.EventHandler(this.comboBoxИсток_DataSourceChanged);
            // 
            // comboBoxСток
            // 
            this.comboBoxСток.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxСток.FormattingEnabled = true;
            this.comboBoxСток.Location = new System.Drawing.Point(7, 144);
            this.comboBoxСток.Name = "comboBoxСток";
            this.comboBoxСток.Size = new System.Drawing.Size(121, 21);
            this.comboBoxСток.TabIndex = 21;
            this.comboBoxСток.SelectedIndexChanged += new System.EventHandler(this.comboBoxСток_SelectedIndexChanged);
            this.comboBoxСток.DataSourceChanged += new System.EventHandler(this.comboBoxСток_DataSourceChanged);
            // 
            // buttonУдалитьСеть
            // 
            this.buttonУдалитьСеть.Location = new System.Drawing.Point(6, 474);
            this.buttonУдалитьСеть.Name = "buttonУдалитьСеть";
            this.buttonУдалитьСеть.Size = new System.Drawing.Size(100, 23);
            this.buttonУдалитьСеть.TabIndex = 22;
            this.buttonУдалитьСеть.Text = "Удалить сеть";
            this.buttonУдалитьСеть.UseVisualStyleBackColor = true;
            this.buttonУдалитьСеть.Click += new System.EventHandler(this.buttonУдалитьСеть_Click);
            // 
            // tabControlВизуализация
            // 
            this.tabControlВизуализация.Controls.Add(this.tabPageСеть);
            this.tabControlВизуализация.Controls.Add(this.tabPageМаксимальныйПоток);
            this.tabControlВизуализация.Location = new System.Drawing.Point(1, -1);
            this.tabControlВизуализация.Name = "tabControlВизуализация";
            this.tabControlВизуализация.SelectedIndex = 0;
            this.tabControlВизуализация.Size = new System.Drawing.Size(400, 570);
            this.tabControlВизуализация.TabIndex = 23;
            // 
            // tabPageСеть
            // 
            this.tabPageСеть.AutoScroll = true;
            this.tabPageСеть.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageСеть.Controls.Add(this.buttonСохранитьСеть);
            this.tabPageСеть.Controls.Add(this.buttonОбновитьГраф);
            this.tabPageСеть.Controls.Add(this.labelЧислоВершин);
            this.tabPageСеть.Controls.Add(this.buttonУдалитьСеть);
            this.tabPageСеть.Controls.Add(this.textBoxЧислоВершин);
            this.tabPageСеть.Controls.Add(this.comboBoxСток);
            this.tabPageСеть.Controls.Add(this.buttonДобавитьРебро);
            this.tabPageСеть.Controls.Add(this.buttonЗадатьЧислоВершин);
            this.tabPageСеть.Controls.Add(this.labelРебраСети);
            this.tabPageСеть.Controls.Add(this.buttonЗагрузить);
            this.tabPageСеть.Controls.Add(this.dataGridViewТаблицаРебер);
            this.tabPageСеть.Controls.Add(this.comboBoxИсток);
            this.tabPageСеть.Controls.Add(this.labelИсток);
            this.tabPageСеть.Controls.Add(this.labelСток2);
            this.tabPageСеть.Location = new System.Drawing.Point(4, 22);
            this.tabPageСеть.Name = "tabPageСеть";
            this.tabPageСеть.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageСеть.Size = new System.Drawing.Size(392, 544);
            this.tabPageСеть.TabIndex = 0;
            this.tabPageСеть.Text = "Сеть";
            // 
            // buttonСохранитьСеть
            // 
            this.buttonСохранитьСеть.Location = new System.Drawing.Point(113, 445);
            this.buttonСохранитьСеть.Name = "buttonСохранитьСеть";
            this.buttonСохранитьСеть.Size = new System.Drawing.Size(96, 23);
            this.buttonСохранитьСеть.TabIndex = 25;
            this.buttonСохранитьСеть.Text = "Сохранить сеть";
            this.buttonСохранитьСеть.UseVisualStyleBackColor = true;
            this.buttonСохранитьСеть.Click += new System.EventHandler(this.buttonСохранитьСеть_Click);
            // 
            // buttonОбновитьГраф
            // 
            this.buttonОбновитьГраф.Location = new System.Drawing.Point(276, 511);
            this.buttonОбновитьГраф.Name = "buttonОбновитьГраф";
            this.buttonОбновитьГраф.Size = new System.Drawing.Size(110, 23);
            this.buttonОбновитьГраф.TabIndex = 24;
            this.buttonОбновитьГраф.Text = "Обновить граф";
            this.buttonОбновитьГраф.UseVisualStyleBackColor = true;
            this.buttonОбновитьГраф.Click += new System.EventHandler(this.buttonОбновитьГраф_Click);
            // 
            // tabPageМаксимальныйПоток
            // 
            this.tabPageМаксимальныйПоток.AutoScroll = true;
            this.tabPageМаксимальныйПоток.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageМаксимальныйПоток.Controls.Add(this.richTextBoxРезультат);
            this.tabPageМаксимальныйПоток.Controls.Add(this.labelРезультат);
            this.tabPageМаксимальныйПоток.Controls.Add(this.panel1);
            this.tabPageМаксимальныйПоток.Controls.Add(this.buttonУдалитьРешение);
            this.tabPageМаксимальныйПоток.Controls.Add(this.labelХодРешения);
            this.tabPageМаксимальныйПоток.Controls.Add(this.buttonНайтиПоток);
            this.tabPageМаксимальныйПоток.Controls.Add(this.richTextBoxХодРешения);
            this.tabPageМаксимальныйПоток.Location = new System.Drawing.Point(4, 22);
            this.tabPageМаксимальныйПоток.Name = "tabPageМаксимальныйПоток";
            this.tabPageМаксимальныйПоток.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageМаксимальныйПоток.Size = new System.Drawing.Size(392, 544);
            this.tabPageМаксимальныйПоток.TabIndex = 1;
            this.tabPageМаксимальныйПоток.Text = "Максимальный поток";
            // 
            // richTextBoxРезультат
            // 
            this.richTextBoxРезультат.BackColor = System.Drawing.SystemColors.Window;
            this.richTextBoxРезультат.Location = new System.Drawing.Point(7, 401);
            this.richTextBoxРезультат.Name = "richTextBoxРезультат";
            this.richTextBoxРезультат.ReadOnly = true;
            this.richTextBoxРезультат.Size = new System.Drawing.Size(323, 56);
            this.richTextBoxРезультат.TabIndex = 21;
            this.richTextBoxРезультат.Text = "";
            // 
            // labelРезультат
            // 
            this.labelРезультат.AutoSize = true;
            this.labelРезультат.Location = new System.Drawing.Point(7, 385);
            this.labelРезультат.Name = "labelРезультат";
            this.labelРезультат.Size = new System.Drawing.Size(59, 13);
            this.labelРезультат.TabIndex = 20;
            this.labelРезультат.Text = "Результат";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.labelПоказатьНаГрафе);
            this.panel1.Controls.Add(this.radioButtonКонечноеРешение);
            this.panel1.Controls.Add(this.radioButtonХодРешения);
            this.panel1.Location = new System.Drawing.Point(8, 463);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(235, 75);
            this.panel1.TabIndex = 19;
            // 
            // labelПоказатьНаГрафе
            // 
            this.labelПоказатьНаГрафе.AutoSize = true;
            this.labelПоказатьНаГрафе.Location = new System.Drawing.Point(12, 10);
            this.labelПоказатьНаГрафе.Name = "labelПоказатьНаГрафе";
            this.labelПоказатьНаГрафе.Size = new System.Drawing.Size(105, 13);
            this.labelПоказатьНаГрафе.TabIndex = 20;
            this.labelПоказатьНаГрафе.Text = "Показать на графе";
            // 
            // radioButtonКонечноеРешение
            // 
            this.radioButtonКонечноеРешение.AutoSize = true;
            this.radioButtonКонечноеРешение.Location = new System.Drawing.Point(15, 49);
            this.radioButtonКонечноеРешение.Name = "radioButtonКонечноеРешение";
            this.radioButtonКонечноеРешение.Size = new System.Drawing.Size(120, 17);
            this.radioButtonКонечноеРешение.TabIndex = 18;
            this.radioButtonКонечноеРешение.Text = "Конечное решение";
            this.radioButtonКонечноеРешение.UseVisualStyleBackColor = true;
            // 
            // radioButtonХодРешения
            // 
            this.radioButtonХодРешения.AutoSize = true;
            this.radioButtonХодРешения.Checked = true;
            this.radioButtonХодРешения.Location = new System.Drawing.Point(15, 26);
            this.radioButtonХодРешения.Name = "radioButtonХодРешения";
            this.radioButtonХодРешения.Size = new System.Drawing.Size(217, 17);
            this.radioButtonХодРешения.TabIndex = 17;
            this.radioButtonХодРешения.TabStop = true;
            this.radioButtonХодРешения.Text = "Ход решения и минимальный  разрез";
            this.radioButtonХодРешения.UseVisualStyleBackColor = true;
            this.radioButtonХодРешения.CheckedChanged += new System.EventHandler(this.radioButtonХодРешения_CheckedChanged);
            // 
            // buttonУдалитьРешение
            // 
            this.buttonУдалитьРешение.Location = new System.Drawing.Point(270, 515);
            this.buttonУдалитьРешение.Name = "buttonУдалитьРешение";
            this.buttonУдалитьРешение.Size = new System.Drawing.Size(119, 23);
            this.buttonУдалитьРешение.TabIndex = 16;
            this.buttonУдалитьРешение.Text = "Удалить решение";
            this.buttonУдалитьРешение.UseVisualStyleBackColor = true;
            this.buttonУдалитьРешение.Click += new System.EventHandler(this.buttonУдалитьРешение_Click);
            // 
            // labelХодРешения
            // 
            this.labelХодРешения.AutoSize = true;
            this.labelХодРешения.Location = new System.Drawing.Point(5, 14);
            this.labelХодРешения.Name = "labelХодРешения";
            this.labelХодРешения.Size = new System.Drawing.Size(73, 13);
            this.labelХодРешения.TabIndex = 15;
            this.labelХодРешения.Text = "Ход решения";
            // 
            // buttonНайтиПоток
            // 
            this.buttonНайтиПоток.Enabled = false;
            this.buttonНайтиПоток.Location = new System.Drawing.Point(270, 486);
            this.buttonНайтиПоток.Name = "buttonНайтиПоток";
            this.buttonНайтиПоток.Size = new System.Drawing.Size(119, 23);
            this.buttonНайтиПоток.TabIndex = 14;
            this.buttonНайтиПоток.Text = "Найти поток";
            this.buttonНайтиПоток.UseVisualStyleBackColor = true;
            this.buttonНайтиПоток.Click += new System.EventHandler(this.buttonНайтиПоток_Click);
            // 
            // richTextBoxХодРешения
            // 
            this.richTextBoxХодРешения.BackColor = System.Drawing.SystemColors.Window;
            this.richTextBoxХодРешения.Location = new System.Drawing.Point(6, 30);
            this.richTextBoxХодРешения.Name = "richTextBoxХодРешения";
            this.richTextBoxХодРешения.ReadOnly = true;
            this.richTextBoxХодРешения.Size = new System.Drawing.Size(324, 334);
            this.richTextBoxХодРешения.TabIndex = 13;
            this.richTextBoxХодРешения.Text = "";
            // 
            // labelГрафСети
            // 
            this.labelГрафСети.AutoSize = true;
            this.labelГрафСети.Location = new System.Drawing.Point(407, 5);
            this.labelГрафСети.Name = "labelГрафСети";
            this.labelГрафСети.Size = new System.Drawing.Size(59, 13);
            this.labelГрафСети.TabIndex = 27;
            this.labelГрафСети.Text = "Граф сети";
            // 
            // pictureBoxГраф
            // 
            this.pictureBoxГраф.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBoxГраф.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxГраф.Location = new System.Drawing.Point(407, 21);
            this.pictureBoxГраф.Name = "pictureBoxГраф";
            this.pictureBoxГраф.Size = new System.Drawing.Size(700, 485);
            this.pictureBoxГраф.TabIndex = 26;
            this.pictureBoxГраф.TabStop = false;
            this.pictureBoxГраф.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxГраф_Paint);
            this.pictureBoxГраф.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxГраф_MouseDown);
            this.pictureBoxГраф.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBoxГраф_MouseMove);
            this.pictureBoxГраф.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxГраф_MouseUp);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1120, 567);
            this.Controls.Add(this.labelГрафСети);
            this.Controls.Add(this.pictureBoxГраф);
            this.Controls.Add(this.tabControlВизуализация);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Поиск максимального потока в сети";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewТаблицаРебер)).EndInit();
            this.tabControlВизуализация.ResumeLayout(false);
            this.tabPageСеть.ResumeLayout(false);
            this.tabPageСеть.PerformLayout();
            this.tabPageМаксимальныйПоток.ResumeLayout(false);
            this.tabPageМаксимальныйПоток.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxГраф)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonЗагрузить;
        private System.Windows.Forms.DataGridViewTextBoxColumn ПропускнаяСпособность;
        private System.Windows.Forms.DataGridViewComboBoxColumn ВтораяВершина;
        private System.Windows.Forms.DataGridViewComboBoxColumn ПерваяВершина;
        private System.Windows.Forms.DataGridView dataGridViewТаблицаРебер;
        private System.Windows.Forms.Label labelРебраСети;
        private System.Windows.Forms.TextBox textBoxЧислоВершин;
        private System.Windows.Forms.Button buttonЗадатьЧислоВершин;
        private System.Windows.Forms.Button buttonДобавитьРебро;
        private System.Windows.Forms.Label labelЧислоВершин;
        private System.Windows.Forms.Label labelСток2;
        private System.Windows.Forms.Label labelИсток;
        private System.Windows.Forms.ComboBox comboBoxИсток;
        private System.Windows.Forms.ComboBox comboBoxСток;
        private System.Windows.Forms.Button buttonУдалитьСеть;
        private System.Windows.Forms.TabControl tabControlВизуализация;
        private System.Windows.Forms.TabPage tabPageСеть;
        private System.Windows.Forms.TabPage tabPageМаксимальныйПоток;
        private System.Windows.Forms.DataGridViewTextBoxColumn Поток;
        private System.Windows.Forms.Button buttonОбновитьГраф;
        private System.Windows.Forms.Button buttonУдалитьРешение;
        private System.Windows.Forms.Label labelХодРешения;
        private System.Windows.Forms.Button buttonНайтиПоток;
        private System.Windows.Forms.RichTextBox richTextBoxХодРешения;
        private System.Windows.Forms.Label labelГрафСети;
        private System.Windows.Forms.PictureBox pictureBoxГраф;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButtonКонечноеРешение;
        private System.Windows.Forms.RadioButton radioButtonХодРешения;
        private System.Windows.Forms.Label labelПоказатьНаГрафе;
        private System.Windows.Forms.Button buttonСохранитьСеть;
        private System.Windows.Forms.RichTextBox richTextBoxРезультат;
        private System.Windows.Forms.Label labelРезультат;
    }
}

