﻿using System;
using System.Drawing;

namespace MaximumNetworkFlow
{
    /// <summary>
    /// Силовые характеристики точек
    /// </summary>
    class PointsForceCharacteristics
    {
        public const int COEFF_ATTRACTION = 2;
        public const int COEFF_REPULSIVE = 1;
        public const int IDEAL_SPRING_LENGTH = 1;
        public const float DELTA = 0.1F;

        /// <summary>
        /// Площадь пространства
        /// </summary>
        public int Area { get; set; }

        /// <summary>
        /// Число точек
        /// </summary>
        public int PointsCount { get; set; }

        /// <summary>
        /// Длина идеального расстояния между вершинами для данной площади и числа вершин 
        /// </summary>
        public float IdealSpringLength
        {
            get
            {
                return (float)Math.Sqrt(Area / PointsCount);
            }
        }


        /// <param name="area">Площадь пространства</param>
        /// <param name="pointCount">Число точек</param>
        public PointsForceCharacteristics(int area, int pointCount)
        {
            Area = area;
            PointsCount = pointCount;
        }

        /// <summary>
        /// Сила притяжения между точками
        /// </summary>
        /// <param name="firstPoint">первая вершина</param>
        /// <param name="secondPoint">вторая вершина</param>
        public PointF AttractionForce(Point firstPoint, Point secondPoint)
        {
            float coeff = (float)(COEFF_ATTRACTION * Math.Log(PointsSpaceCharacteristics.Distance(firstPoint, secondPoint) / IDEAL_SPRING_LENGTH));

            PointF vector = PointsSpaceCharacteristics.NormDifference(secondPoint, firstPoint);

            return new PointF(coeff * vector.X, coeff * vector.Y);
        }


        /// <summary>
        /// Сила отталкивания между точками
        /// </summary>
        /// <param name="firstPoint">первая точка</param>
        /// <param name="secondPoint">вторая точка</param>
        public PointF RepulsiveForce(Point firstPoint, Point secondPoint)
        {
            float coeff = (float)(COEFF_REPULSIVE / Math.Pow(PointsSpaceCharacteristics.Distance(secondPoint, firstPoint), 2));

            PointF vector = PointsSpaceCharacteristics.NormDifference(firstPoint, secondPoint);

            return new PointF(coeff * vector.X, coeff * vector.Y);
        }
    }
}