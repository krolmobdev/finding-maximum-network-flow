﻿using System;
using System.Collections.Generic;

namespace MaximumNetworkFlow
{
    /// <summary>
    /// Реализует алгоритм Форда-Фалькерсона поиска максимального потока в сети
    /// </summary>
    class FordFalkersonAlgorithm
    {
        /// <summary>
        /// Принадлежит ли путь s->v отстаточному графу
        /// </summary>
        private bool[] marked;

        /// <summary>
        /// Последнее ребро в пути s->v
        /// </summary>
        private FlowEdge[] edgeTo;

        /// <summary>
        /// Пошаговый поток в сети
        /// </summary>
        private List<HashSet<FlowEdge>> stepsFlow = new List<HashSet<FlowEdge>>();

        /// <summary>
        /// Текущее значение максимального потока
        /// </summary>
        public double Value { get; private set; }

        /// <summary>
        /// Протокол работы алгоритма
        /// </summary>
        public string Protocol { get; private set; }

        /// <summary>
        /// Сеть в максимальным потоком
        /// </summary>
        public FlowNetwork Network { get; private set; }


        /// <summary>
        /// Пошаговый поток: содержит список состоящий из ребер сети на каждой итерации алгоритма
        /// </summary>
        public List<HashSet<FlowEdge>> StepsFlowEdge
        {
            get
            {
                return stepsFlow;
            }
        }

        /// <summary>
        /// Минимальный разрез
        /// </summary>
        public List<FlowEdge> MinCut
        {
            get
            {
                List<FlowEdge> cutEdges = new List<FlowEdge>();
                List<FlowEdge> edges = Network.Edges;

                for (int v = 0; v < Network.VertexCount; v++)
                {
                    if (InCut(v))
                    {
                        foreach (FlowEdge edge in edges)
                        {
                            if (edge.From == v || edge.To == v)
                            {
                                int otherVertex = edge.OtherVertex(v);
                                if (!InCut(otherVertex))
                                {
                                    cutEdges.Add(edge);
                                }
                            }
                        }
                    }
                }
                return cutEdges;
            }
        }

        /// <summary>
        /// Пропускная способность минимального разреза
        /// </summary>
        public double MinCutCapacity
        {
            get
            {
                double capacity = 0;
                foreach (FlowEdge edge in MinCut)
                {
                    if (InCut(edge.From))
                    {
                        capacity += edge.Capacity;
                    }
                }

                return capacity;
            }
        }

        /// <summary>
        /// Создает объект алгоритм и находит максимальный поток в сети 
        /// </summary>
        public FordFalkersonAlgorithm(FlowNetwork network)
        {
            Network = network;

            Protocol = "";
            Value = 0;
            stepsFlow.Clear();

            FindMaxFlow();
        }

        /// <summary>
        /// Нахождение максимального пути в транспортной сети G из s в t
        /// </summary>
        private void FindMaxFlow()
        {
            Protocol += "Исходная сеть";
            Protocol += "\n" + Network;
            Protocol += "\nВеличина потока:" + Value;

            stepsFlow.Add(new HashSet<FlowEdge>(Network.Copy.Edges));


            int iter = 1;

            //Пока существует расширяющийся путь, используем его для увеличения потока
            while (HasAugmentingPath())
            {
                //Вычисление минимальной пропускной способности на которую можно увеличить поток по расширяющемуся пути

                double delta = double.MaxValue;//Задание начального значения величины увеличения потока по расширяющемуся пути 

                for (int vertex = Network.Target; vertex != Network.Source; vertex = edgeTo[vertex].OtherVertex(vertex))
                {
                    delta = Math.Min(delta, edgeTo[vertex].ResidualCapacityTo(vertex));
                }

                //Расширение пути

                for (int vertex = Network.Target; vertex != Network.Source; vertex = edgeTo[vertex].OtherVertex(vertex))
                {
                    edgeTo[vertex].AddResidualFlowTo(vertex, delta);
                }
                Value += delta;

                //Записываем результаты работы алгоритма
                stepsFlow.Add(new HashSet<FlowEdge>(Network.Copy.Edges));

                Protocol += "\n\nШаг №" + (iter++);
                Protocol += "\n" + Network;
                Protocol += "\nВеличина потока:" + Value;
            }
        }

        /// <summary>
        /// Нахождение расширяющего пути в остаточной сети с помощью поиска в ширину
        /// </summary>
        ///<param name="network">поточная сеть</param>
        /// <returns>Существует ли путь из вершины source в вершину target?</returns>
        private bool HasAugmentingPath()
        {
            marked = new bool[Network.VertexCount];
            edgeTo = new FlowEdge[Network.VertexCount];

            //очередь посещаемых вершин
            Queue<int> vertexQueue = new Queue<int>();

            //пометка источника и занесение его в очередь посещаемых вершин
            marked[Network.Source] = true;
            vertexQueue.Enqueue(Network.Source);

            while (vertexQueue.Count != 0)
            {
                int vertex = vertexQueue.Dequeue();

                foreach (FlowEdge edge in Network.AdjacentEdges(vertex))
                {
                    int adjacentVertex = edge.OtherVertex(vertex);

                    if (edge.ResidualCapacityTo(adjacentVertex) > 0 && !marked[adjacentVertex])
                    {
                        //Для каждого ребра к непомеченным вершинам (в остаточной сети)
                        edgeTo[adjacentVertex] = edge;  //Сохранение последнего ребра в пути.
                        marked[adjacentVertex] = true;  //Пометка adjacentVertex,т.к путь известен,
                        vertexQueue.Enqueue(adjacentVertex); //и занесение его в очередь
                    }
                }
            }
            return marked[Network.Target];
        }

        ///<summary>
        /// Возвращает пошаговые значения потока ребра на разных итерациях алгоритма
        /// </summary>
        /// <returns></returns>
        public List<double> GetEdgeStepFlow(FlowEdge edge)
        {
            List<double> result = new List<double>();
            foreach (HashSet<FlowEdge> edges in StepsFlowEdge)
            {
                foreach (FlowEdge e in edges)
                {
                    if (e.Equals(edge))
                    {
                        result.Add(e.Flow);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Попадает ли вершина в разрез
        /// </summary>
        public bool InCut(int vertex)
        {
            if (vertex > marked.Length && vertex < 0)
            {
                throw new IndexOutOfRangeException(nameof(vertex));
            }

            return marked[vertex];
        }

        /// <summary>
        /// Проверка локального баланса в вершине
        /// </summary>
        private bool LocalBalance(int vertex)
        {
            double EPSILON = 1E-11;
            double netflow = 0;

            foreach (FlowEdge edge in Network.AdjacentEdges(vertex))
            {
                if (vertex == Network.Source || vertex == Network.Target)
                {
                    continue;
                }

                if (vertex == edge.From)
                {
                    netflow -= edge.Flow;
                }
                else
                {
                    netflow += edge.Flow;
                }
            }

            return Math.Abs(netflow) < EPSILON;
        }

        /// <summary>
        /// Проверка локального баланса в каждой вершине.
        /// Возвращает вершины, в которых нарушен баланс
        /// </summary>
        public List<int> LocalBalance()
        {
            List<int> notBalancedVertices = new List<int>();

            for (int v = 0; v < Network.VertexCount; v++)
            {
                if (!LocalBalance(v))
                {
                    notBalancedVertices.Add(v);
                }

            }

            return notBalancedVertices;
        }

        /// <summary>
        /// Проверка, что поток в каждом ребре больше нуля и не превышает пропускной способности ребра.
        /// Возвращает ребра, в которых нарушен баланс
        /// </summary>
        public List<FlowEdge> IsFeasibleFlow()
        {
            List<FlowEdge> edges = Network.Edges;
            List<FlowEdge> notFeasibleEdges = new List<FlowEdge>();

            for (int v = 0; v < Network.VertexCount; v++)
            {
                foreach (FlowEdge edge in edges)
                {
                    if (edge.Flow < 0 || edge.Flow > edge.Capacity)
                    {
                        notFeasibleEdges.Add(edge);
                    }
                }
            }

            return notFeasibleEdges;
        }
    }
}
