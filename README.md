Программа поиска максимального потока и минимального разреза в сети 
(Лабораторная работа по методам оптимизации для ЧГУ)

- позволяет загружать данные сети из текстового файла и редактировать их на форме
- отображает граф сети
- вычисляет максимальный поток по алгоритму Форда-Фалкерсона
- находит минимальный разрез в сети